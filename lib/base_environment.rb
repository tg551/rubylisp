module Lisp
  def make_preseeded_env
    base_env = make_base_env
    Lisp::instance_methods(false).each do |m|
      the_method = Lisp::instance_method(m)
      args = the_method.arity.times.map {|i| (i+97).chr}
      arg_list_for_eval = args.map {|a| "@#{a}"}.join " "
      data = "#{the_method.name} #{arg_list_for_eval}"
      the_lambda = ruby_code_lambda(data)
      add_to_env base_env,
        legalise_ivar_name(the_method.name.to_s),
        LispLambda.new(the_lambda, args, base_env, "")
    end
    base_env
  end
  
  def legalise_ivar_name name
    name = "#{name[0..-2]}_query" if name.end_with? "?"
    name = "#{name[0..-2]}_bang" if name.end_with? "!"
    name
  end
  
end