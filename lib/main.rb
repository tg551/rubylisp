RubyVM::InstructionSequence.compile_option = {
  :tailcall_optimization => true,
  :trace_instruction => false
}

require_relative 'parser'
require_relative 'eval'
require 'pp'


module GoModule
  class Go
    include Lisp
    def initialize
      
      preprocessed = read_and_preprocess 'sourcecode'
      #pp preprocessed
      tokens = lex preprocessed
      #pp tokens
      ast = parse tokens
      #pp ast

      eval_program ast
    end
  end
  Go.new
end