require 'English'
module Lisp
  class LexerDebugException < StandardError; end
  class LexerException < StandardError; end

  Struct.new "Token", "name", "data", "file", "line", "col"
  Struct.new "TokenType", "regex", "symbol", "discard"

  class RemainingSource
    attr_reader :line, :col, :source
    def initialize source
      @source = source
      @line = 1
      @col = 1
    end
  
    def eat length
      match_text = @source[0..length-1]
      @source = @source[length..-1]
      match_text.each_char do |char|
        @col += 1
        if char == "\n"
          @line += 1 
          @col = 1
        end
      end
    end
  end
  
  SourceLineWithFile = Struct.new :file, :line, :data
  
  def read_and_preprocess sourcefile
    data = File.read sourcefile
    lines = data.split("\n")
    lines.each_with_index.with_object([]) do |(text, line), ret|
      #sourcecode is 1-indexed
      line = line + 1
      
      #check for text inclusion command. If so, then add it to the results
      m = text.match /\A\-include\s+(\S+)\z/
      if m.is_a? MatchData
        read_and_preprocess(m.captures.first).each {|x| ret << x}
      else
        ret << SourceLineWithFile.new(sourcefile, line, text)
      end
    end
  end

  def lex preprocessed_source
    #remaining = RemainingSource.new source
    token_defs = self.read_lexer_data
    preprocessed_source.each_with_object([]) do |preprocessed, tokens|
      remaining = preprocessed.data
      col = 0
      until remaining.empty?
        success_type = nil
        matchdata = nil
        
        token_defs.each do |tt| 
          matchdata = tt.regex.match remaining
          success_type = tt
          break if matchdata 
        end
        if success_type.nil? || matchdata.nil?
          msg = "Unable to lex: \"#{remaining.source[0..10]}\"..., " <<
            "line #{remaining.line}, col #{remaining.col}"
          raise LexerException, msg
        end
        
        data = matchdata[0]
        if data.length.zero?
          msg = "Zero length occurred in lexer: Regexp " <<
            "#{success_type.regex} matched the beginning of " <<
            "#{remaining.source[0..10]}...\""
          raise LexerDebugException, msg
        end
      
        line = preprocessed.line
        col += matchdata[0].length
        remaining = remaining[matchdata[0].length..-1]
        tokens << Struct::Token.new(success_type.symbol, data, preprocessed.file, line, col) unless success_type.discard
      end
    end
  end
  
  def parse tokens
    self.real_parse(tokens).first
  end
    
  def real_parse tokens
    ast = []
    until tokens.empty? do
      token = tokens.shift
      return ast if token.name == :closbr
      if token.name == :openbr
        ast << self.real_parse(tokens)
      else
        ast << token
      end
    end
    ast
  end
  
  def read_lexer_data
    File.read('lexer_data').lines.map(&:strip).map(&:split).map do |first, last, *args|
      match_newline = args.include? "match_newline"
      regex = Regexp.compile '\A' + first, if match_newline then Regexp::MULTILINE end
      name = last.to_sym
      discard_boolean = args.include? "discard"
      Struct::TokenType.new regex, name, discard_boolean
    end
  end
end
