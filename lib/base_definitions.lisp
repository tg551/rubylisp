(define true
  ((ruby-lambda () #RB#true#RB#)))

(define false
  ((ruby-lambda () #RB#false#RB#)))

(define plus 
  (ruby-lambda (a b) #RB#ret = @a + @b#RB#))

(define mod
  (ruby-lambda (a b) #RB#@a % @b#RB#))

(define send 
  (ruby-lambda (obj msg) #RB#@obj.send @msg#RB#))

(define print
  (ruby-lambda (a b) #RB#puts @a; @a#RB#))

(define pp
  (ruby-lambda (a b) #RB#pp @a; @a#RB#))

(define eqeq (ruby-lambda (a b) #RB#@a == @b#RB#))