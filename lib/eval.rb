require_relative 'env'
require_relative 'base_environment'

module Lisp
  LispLambda = Struct.new :actual_lambda, :args, :env, :rb_sourcecode
  CallRubyProcInLispEnvFromRuby = Struct.new :proc_name, :args
  
  def eval_program ast
    catch(:done) do
      eval_ast ast, make_env(make_preseeded_env)
      #eval_ast ast, make_env(make_base_env)
    end
  end
  
  def woo arg1
    puts arg1.inspect
    return arg1
  end

  def eval_ast ast, env
    case
    when sequence?(ast)
      #eval_sequence ast, env
      #return CallRubyProcInLispEnvFromRuby.new "eval_sequence", [ast,env]
    when string?(ast)
      string_evaluate ast
    when number?(ast)
      number_evaluate ast
    when end?(ast)
      throw :done
    when var_definition?(ast)
      define_variable ast, env
    when ruby_lambda?(ast)
      make_ruby_lambda ast, env
    when print_env?(ast)
      print_env env
    when if?(ast)
      do_if ast, env
    when var_lookup?(ast)
      lookup_var ast, env
    when lambda?(ast)
      make_lambda ast, env
    when application?(ast)
      apply ast, env
    else
      raise "Unable to evaluate #{ast.first}"
    end
  end

  def sequence? ast
    return false if ast.first.is_a? Array
    ret = ast.first.name == :identifier &&
      ast.first.data == "sequence"
    ret
  rescue
    false
  end

  def end? ast
    return false if ast.is_a? Array
    ast.name == :end
  end

  def var_definition? ast
    return false unless ast.is_a? Array
    return false if ast.first.is_a? Array
    ast.first.name == :identifier &&
      ast.first.data == "define"
  end

  def define_variable ast, env
    variable = ast[1].data
    value = ast[2]
    add_to_env env, variable, eval_ast(value, env)
  end

  def string? ast
    return false if ast.is_a? Array
    ast.name == :string
  end

  def string_evaluate ast
    ast.data[1..-2]
  end

  def number? ast
    return false if ast.is_a? Array
    ast.name == :number
  end

  def number_evaluate ast
    ast.data.to_i
  end

  def eval_sequence ast, env
    ret = nil
    ast[1..-1].each {|ast| ret = eval_ast ast, env}
    ret
  end

  def print_env? ast
    return false if ast.is_a? Array
    ast.data == "print-env"
  end

  def print_env env
    pp env
  end

  def ruby_lambda? ast
    return false unless ast.is_a? Array
    return false if ast.first.is_a? Array
    ast.first.name == :identifier &&
      ast.first.data == "ruby-lambda"
  end

  def make_ruby_lambda ast, env  
    arg_list = ast[1].map do |identifier_token|
      identifier_token.data
    end
    ruby_source = ast[2].data.gsub('#RB#', "")
    
    ruby_lambda = ruby_code_lambda ruby_source
    LispLambda.new ruby_lambda, arg_list, env, ruby_source
  end

  def ruby_code_lambda ruby_source
    enclosing_obj = Object.new
    enclosing_obj.instance_eval do
      
      #Gives access to all methods defined in the ruby Lisp module
      class << self; include Lisp; end
      
      lambda do |env|
      
        #Put all non lisp variables in the environment of the ruby object,
        #so that they are available as instance vars for the ruby eval process.
        all_variables_in_env(env).each_pair do |identifier, value|
          identifier = identifier
          instance_variable_set "@#{identifier}", value
        end
        eval ruby_source
      end
    end
  end

  def application? ast
    ast.is_a? Array
  end

  def apply ast, env
    procedure_env_pair = eval_ast ast[0], env
    actual_procedure = procedure_env_pair[0]
    argument_names = procedure_env_pair[1]
    argument_values = ast[1..-1].map {|ast| eval_ast ast, env}
    application_env = make_application_env env, argument_names, argument_values
  
    if actual_procedure.is_a? Proc
      apply_ruby_proc actual_procedure, application_env
    else
      apply_lisp_proc actual_procedure, application_env
    end
  end

  def make_application_env parent_env, argument_names, argument_values
    the_env = make_env parent_env
    argument_names.zip(argument_values).each do |name, value|
      add_to_env the_env, name, value
    end
    the_env
  end

  def apply_ruby_proc proc, env
    proc.call env
  end

  def apply_lisp_proc proc, env
    eval_ast proc, env
  end

  def var_lookup? ast
    return false if ast.is_a? Array
    ast.name == :identifier
  end

  def lookup_var ast, env
    lookup_in_env env, ast.data
  end

  def lambda? ast
    return false unless ast.is_a? Array
    return false if ast.first.is_a? Array
    ast.first.name == :identifier &&
      ast.first.data == "lambda"
  end

  def make_lambda ast, env
    arg_list = ast[1].map &:data
    evaluate_later = ast[2]
    LispLambda.new evaluate_later, arg_list, env, ""
  end
end

def if? ast
  return false unless ast.is_a? Array
  return false if ast.first.is_a? Array
  ast.first.name == :identifier &&
    ast.first.data == "if"
end

def do_if ast, env
  test = ast[1]
  true_part = ast[2]
  false_part = ast[3]
  
  if eval_ast test, env
    eval_ast true_part, env
  else
    eval_ast false_part, env
  end
end