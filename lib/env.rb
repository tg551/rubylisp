module Lisp
  def make_base_env
    make_env nil
  end

  def make_env parent_env
    bindings = {}
    [parent_env, bindings]
  end

  def add_to_env env, name, value
    own_bindings = env[1]
    own_bindings[name] = value
  end

  def lookup_in_env env, name
    current_bindings = env[1]
    found_value = current_bindings[name]
    return found_value unless found_value.nil?
  
    parent_env = env[0]
    raise "environment error: Could not find #{name}" if parent_env.nil?
    lookup_in_env parent_env, name
  end

  def all_variables_in_env env
    all_variables_in_env_inner env, {}
  end

  def all_variables_in_env_inner env, ret
    all_variables_in_env_inner env[0], ret unless env[0].nil?
    env[1].each_pair {|identifier, value| ret[identifier] = value}
    ret
  end
end