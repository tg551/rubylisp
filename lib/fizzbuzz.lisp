(define many_fizzbuzz
    (lambda (current max)
      (sequence
         (define fizzbuzz
           (lambda (i)
             (print (if (eqeq (mod i 15) 0)
                        "fizzbuzz"
                        (if (eqeq (mod i 5) 0)
                            "buzz"
                            (if (eqeq (mod i 3) 0)
                                "fizz"
                                 i))))))
          (if (eqeq current max)
              ""
              (sequence (fizzbuzz current)
                        (many_fizzbuzz (plus current 1) max))))))


#(fizzbuzz 30)
(many_fizzbuzz 0 100)
